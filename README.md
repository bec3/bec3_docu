# bec3_docu

 **Created by:** Ines Adriaens (adria036)
    - **Collaborators:** Istvan Fodor, Marjaneh Taghavi, Roxann Rikkers
- **Created on:** May 24, 2022

For questions, please contact [Ines Adriaens](mailto:ines.adriaens@wur.nl)


## LINK TO MANUSCRIPT - DRAFT

[Manuscript google docs](https://docs.google.com/document/d/1khgZ5boM8UMFYkfutPi-PTG1NkdrHMFexrKTl37BmAA/edit?usp=sharing)

## Project description

Smart & data driven solutions for climate adaptation actions.  
Funded by Investment theme *"Data driven discoveries in a changing climate"*

## Project structure + roadmap

1. Project initialisation and plan (this repository)
2. Code modules:
    - Preprocessing (+ generalisation / requirements) - data scripts
    - Analysis of dataset 1 (spatial and behaviour data)
    - Analysis of dataset 2 (milk production and activity data)
3. Documentation + dissemination
    - Code documentation / docstrings
    - Presentation
	- Draft manuscript

## Scripts - files

- [x] README.md (this file)
- [x] .gitignore (local files not version controlled)

## Using this repo (development only)

**Configuration**
#### Requirements
__Download in this order__:
- [Notepad++](https://notepad-plus-plus.org/downloads/v8.4.1/)
- [Putty/PuttyGen](https://www.puttygen.com/download-putty)
- [Git for windows] (https://gitforwindows.org/)

#### Configuration

1) Create GitLab account on [wur gitlab](https://git.wur.nl/)
2) Open PuttyGen and generate a public-private key pair, for example a ssh-ed25519.
	- save both in a folder on your computer, e.g. in "C:/users/user_name/.ssh/"
	- copy the public key to your GitLab profile in Preferences > SSH Keys > Add an SSH key
		- the format is as follows: "ssh-ed25519 AAAAC3NzaC1lZDI1NTExxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
		- or "ssh-rsa AAAAAxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
3) go to Access Tokens > Add a personal access token > select all options > create. Save your token in a password manager and keep it close
4) For enabling two-factor authentication, scan the code with your authenticator app (plus in right upper corner > other)
	- save the recovery codes in a safe place or passwork manager


#### Connect

1) create group on gitlab for your project
2) create a repository (= project) on GitLab
3) click "clone" + copy link to clone with https
3) open git bash in your local folder where you want to put the repository (right mouse click > Git Bash Here)
4) type "git clone _copied_link_to_repo_ "
5) enter your wur mailadress and NOT the password, but the TOKEN you copied in the previous part step 3.


**Initialisation**
- on your local computer: go to project folder (in my case: C:\Users\adria036\OneDrive - Wageningen University & Research\iAdriaens_doc\Projects\iAdriaens\bait\code\)
- open git Bash (*Git Bash Here*)
- git clone this repo (a new folder "bait" will appear)
```
git clone https://git.wur.nl/iadriaens/bait.git
cd bait
```
  
**Working with git (if alone)**  
- make changes locally and commit them
- this will only keep track of your changes locally (on your own pc)  
- git status = which changes have or have not been committed

```
git status  
git add .
git commit -m "meaningful_message"
```

- to make changes without interference with other people's work
    - BEFORE starting to work: pull last version of the project in "main"
    - create + checkout your local branch
    - "git add ." + "git commit -m "message""
- the changes in your local branch need to be merged into main in the remote
    - push local branch and ask for merge (or merge yourself)

**Working with git (if others work with you)**

```
git pull
git checkout -b new_branch_name
```

commit changes:
```
git status  
git add .
git commit -m "meaningful_message"
```


- to push your local changes to the remote, depending whether you worked in 'main' branch or a local branch
```
git push origin new_branch_name (or main)
```

In the remote, you can now create a merge request to add your changes to the main.


## Contributing

Codes can be shared on request